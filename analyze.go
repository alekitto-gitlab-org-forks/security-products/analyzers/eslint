package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/eslint/v2/convert"
)

const (
	pathESLint   = "/home/node/node_modules/.bin/eslint"
	pathESLintRC = "/home/node/.eslintrc"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// Perform the analysis
	cmd := exec.Command(pathESLint, "-c", pathESLintRC, "--no-eslintrc", "--ext", ".html,.js,.jsx,.ts,.tsx", "-f", "json", ".", "--resolve-plugins-relative-to", "/home/node")
	cmd.Dir = path
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			if waitStatus.ExitStatus() >= 2 {
				// An error occurred while running ESLint
				// (see https://eslint.org/docs/user-guide/command-line-interface#exit-codes )
				log.Errorf("An error occurred while running eslint: %s\n", err)
				return nil, err
			}
		}
	}

	// Make paths relative. This can't be done in convert as we won't have
	// the path argument anymore
	log.Info("Making paths relative")
	jsonByte, err := makePathsRelative(output, path)
	if err != nil {
		log.Errorf("An error occured while making the paths relative: %s\n", err)
		return nil, err
	}

	return ioutil.NopCloser(bytes.NewReader(jsonByte)), nil

}

// makePathsRelative make paths in the report file relative to the given root path
func makePathsRelative(jsonIn []byte, rootPath string) ([]byte, error) {
	var reports []*convert.ESLintFileReport

	err := json.Unmarshal(jsonIn, &reports)
	if err != nil {
		return nil, err
	}

	// Make paths relative
	for _, report := range reports {
		relPath, err := filepath.Rel(rootPath, report.FilePath)
		if err != nil {
			return nil, err
		}

		report.FilePath = relPath
	}

	return json.Marshal(reports)
}
