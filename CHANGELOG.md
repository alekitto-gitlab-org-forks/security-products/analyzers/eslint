# ESLint analyzer changelog

## v2.25.3
- Update analyzer.yaml reference in ci script

## v2.25.2
- Fix CA certs in OpenShift environments by removing node user (!106)

## v2.25.1
- Update dependencies (!100)
  - Update direct Go modules (updates indirect dependencies)
    - `analyzers/command` to `v1.6.1`
    - `analyzers/report` to `v3.10.0`
  - Update Node alpine to `16.14-alpine3.15` in Dockerfile
  - Update minor/patch versions in `package.json`:
    - @babel/cli: `7.17.6`
    - @babel/core: `7.17.5`
    - @babel/plugin-proposal-class-properties: `7.16.7`
    - @babel/plugin-proposal-decorators: `7.17.2`
    - @babel/plugin-proposal-do-expressions: `7.16.7`
    - @babel/plugin-proposal-export-default-from: `7.16.7`
    - @babel/plugin-proposal-export-namespace-from: `7.16.7`
    - @babel/plugin-proposal-function-bind: `7.16.7`
    - @babel/plugin-proposal-function-sent: `7.16.7`
    - @babel/plugin-proposal-json-strings: `7.16.7`
    - @babel/plugin-proposal-logical-assignment-operators: `7.16.7`
    - @babel/plugin-proposal-nullish-coalescing-operator: `7.16.7`
    - @babel/plugin-proposal-numeric-separator: `7.16.7`
    - @babel/plugin-proposal-optional-chaining: `7.16.7`
    - @babel/plugin-proposal-pipeline-operator: `7.17.6`
    - @babel/plugin-proposal-throw-expressions: `7.16.7`
    - @babel/preset-env: `7.16.11`
    - @babel/preset-flow: `7.16.7`
    - @babel/preset-react: `7.16.7`
    - eslint-plugin-react: `7.29.3`

## v2.25.0
- Update ruleset, report, and command modules to support ruleset overrides (!98)

## v2.24.4
- Update dependencies (!97)
    - Update Go dependencies
    - Update `eslint-plugin-html` to [6.2.0](https://www.npmjs.com/package/eslint-plugin-html/v/6.2.0) ([CHANGELOG](https://github.com/BenoitZugmeyer/eslint-plugin-html/blob/89f46c274/CHANGELOG.md))
    - Update `eslint-plugin-react` to [7.28.0](https://www.npmjs.com/package/eslint-plugin-react/v/7.28.0) ([CHANGELOG](https://github.com/yannickcr/eslint-plugin-react/blob/master/CHANGELOG.md#7280---20211222))
        - Add, fix, and update various rules
        - Fix various internal components

## v2.24.3
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!95)
- Bump base container from 16.11 to 16.33 (!96)

## v2.24.2
- Upgrade go to v1.17 (!94)

## v2.24.1
- Update container base image to node:16.11-alpine3.14 (!92)

## v2.24.0
- Update @babel/cli to version 7.15.4
- Update @babel/preset-env to version 7.15.6

## v2.23.0
- Addressed all vulnerabilities reported by DS, SAST and container scanning (!88)
    - Fix: Use a vuln-free alpine docker base image
    - Fix: Upgrade and pin apk-tools to a vuln-free version
    - Fix: Upgrade and pin path-parse to a vuln-free version

## v2.22.0
- Update eslint to [7.32.0](https://www.npmjs.com/package/eslint/v/7.32.0) (!87)
  - For the most part, this release involves some chores, fixes and documentation changes.
  - Fix: ignore lines with empty elements
  - Chore: Simplify internal no-invalid-meta rule
  - Chore: Adopt eslint-plugin/require-meta-docs-url rule internally
- Update supporting babel plugins to their latest versions (!87)

## v2.21.0
- Update eslint to [7.30.0](https://www.npmjs.com/package/eslint/v/7.30.0) (!86)
  - For the most part, this release involves some chores, fixes and documentation changes.
  - Fix: no-duplicate-imports allow unmergeable
  - Fix: no-implicit-coercion false positive with String()
  - Fix: Expose true filename to rules: https://github.com/eslint/eslint/issues/11989
  - Fix: Linter ignores multiline directives: https://github.com/eslint/eslint/issues/14652
- Update supporting babel plugins to their latest versions (!86)

## v2.20.0
- Update eslint to [7.27.0](https://www.npmjs.com/package/eslint/v/7.27.0) (!81)
  - For the most part, this release involves some chores and fixes.
- Update supporting babel plugins to their latest versions

## v2.19.0
- Update @babel/preset-env to [7.14.1](https://www.npmjs.com/package/@babel/preset-env/v/7.14.1) (!77)
- Update supporting babel plugins to their latest versions

## v2.18.0
- Update eslint to [7.25.0]() (!76)
- Update eslint-plugin-react to [7.23.2](https://github.com/yannickcr/eslint-plugin-react/releases/tag/v7.23.2)
  - New rule requiring or preventing new line after JSX elements and expressions
  - New rule checking when value passed to a Context Provider will cause needless rerenders
  - New rule to prevent creating unstable components inside components
- Update eslint-plugin-html to [6.1.2]()
- Update babel/cli to [7.13.16]()
- Update babel/core to [7.12.10]()
- Update supporting babel plugins to their latest versions

## v2.17.0
- Update eslint to [7.23.0](https://github.com/eslint/eslint/releases/tag/v7.23.0) (!75)

## v2.16.0
- Update report dependency in order to use the report schema version 14.0.0 (!74)

## v2.15.0
- Update eslint to [7.21.0](https://github.com/eslint/eslint/releases/tag/v7.21.0) (!73)

## v2.14.0
- Update eslint to 7.19.0 (!70)

## v2.13.0
- Add CWE identifiers and severities to report findings (!68)

## v2.12.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!67)

## v2.12.0
- Update eslint to 7.15.0 (!66)
- Update base image to node15.4-alpine3.12 (!66)
- Update babel/cli to v7.12.10 (!66)
- Update babel/core to v7.12.10 (!66)
- Update babel/plugin-proposal-numeric-separator to v7.12.7 (!66)
- Update babel/plugin-proposal-optional-chaining to v7.12.7 (!66)
- Update babel/preset-env to v7.12.11 (!66)
- Update babel/preset-react to v7.12.10 (!66)

## v2.11.1
- Remove non-security specific react rule (!65)

## v2.11.0
- Upgrade common to v2.22.0 (!64)
- Update urfave/cli to v2.3.0 (!64)

## v2.10.1
- Update babel, eslint, and golang dependencies (!63)

## v2.10.0
- Update common and enable disablement of custom rulesets (!62)

## v2.9.2
- Fix bug which prevented writing `ADDITIONAL_CA_CERT_BUNDLE` value to `/etc/gitconfig` (!52)

## v2.9.1
- Update golang dependencies to latest version (!51)

## v2.9.0
- Update eslint to 7.9.0 (!48)
- Update babel and supporting JS plugins to latest versions (!48)
- Update base docker image to node:14-alpine3.12 (!48)

## v2.8.2
- Upgrade to common v2.18.0 (!50)

## v2.8.1
- Add description for security/detect-new-buffer rule (!47)

## v2.8.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!46)

## v2.7.1
- Update eslint to 7.7.0 (!43)
- Update supporting dependencies
- Upgrade go to version 1.15

## v2.7.0
- Add scan object to report (!40)

## v2.6.1
- Update eslint to 7.5.0 (!35)
- Update supporting dependencies

## v2.6.0
- Switch to the MIT Expat license (!33)

## v2.5.1
- Move CLI info logs to common (!30)

## v2.5.0
- Add typescript support (!31)

## v2.4.1
- Update Debug output to give a better description of command that was ran (!32)

## v2.4.0
- Update logging to be standardized across analyzers (!29)

## v2.3.1
- Remove `location.dependency` from the generated SAST report (!25)

## v2.3.0
- Update base Docker image to utilize node 14 (!23)
- Update eslint to 7.0.0
- Update supporting dependencies

## v2.2.1
- Parse URLs out of messages and add them as Links

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!19)

## v2.1.0
- Add support for custom CA certs (!17)

## v2.0.4
- Use babel-parser to support Stage-0 ES syntax

## v2.0.3
- Add eslint-plugin-react in support of React projects

## v2.0.2
- Update common to v2.1.6

## v2.0.1
- Ignore `.eslintrc` files in the repo

## v2.0.0
- Switch to new report syntax with `version` field

## v1.0.1
- Fix missing `.eslintrc` file when `$HOME` is not set to `/home/node`

## v1.0.0
- Initial release
